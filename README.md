# UE5 Toon Shader

An Unreal Engine toon shader in the style of Moebius. https://www.iamag.co/the-art-of-moebius/

## Project status

* 2023/9/22 Conception
* 2023/9/30 Gitlab Repo Created

## References

* Shader Development | Unreal Engine Documentation https://docs.unrealengine.com/4.26/en-US/ProgrammingAndScripting/Rendering/ShaderDevelopment/
* Adding Global Shaders to Unreal Engine | Unreal Engine 4.27 Documentation https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Rendering/ShaderDevelopment/AddingGlobalShaders/
* Create a New Global Shader as a Plugin | Unreal Engine 4.27 Documentation https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Rendering/ShaderInPlugin/QuickStart/
* Unreal Tutorial - Black and White Shader with Excluded Objects - YouTube https://www.youtube.com/watch?v=nrN4LxY_J7k
* Black and White Shader | UE4 Tutorial - YouTube https://www.youtube.com/watch?v=MWKVdH5qVb4

* Writing your own custom shaders for UE4 : r/unrealengine https://www.reddit.com/r/unrealengine/comments/3s6wqp/writing_your_own_custom_shaders_for_ue4/?rdt=46373
* Unreal Engine 4 Custom Shaders Tutorial | Kodeco https://www.kodeco.com/57-unreal-engine-4-custom-shaders-tutorial
* How to get started writing custom shaders (esp in unreal) - Real Time VFX https://realtimevfx.com/t/how-to-get-started-writing-custom-shaders-esp-in-unreal/22809
* Writing shaders for UE4; where do I start? - Development / Rendering - Epic Developer Community Forums https://forums.unrealengine.com/t/writing-shaders-for-ue4-where-do-i-start/52259/5
* How to Make a Toon Shader in UE5 - YouTube https://www.youtube.com/watch?v=mzydOmgN7mc
* Shaders in UE4 | Live Training | Unreal Engine Livestream - YouTube https://www.youtube.com/watch?v=mig6EF17mR8
* Unreal Engine 5 Tutorial - Technical Shading - Introduction To HLSL - YouTube https://www.youtube.com/watch?v=lsXB1PQdGx0
* Moebius-style 3D Rendering https://www.youtube.com/watch?v=jlKNOirh66E
* Toon Shader & Custom Shadow - UE4 Postprocess Tutorial [ UE5 Valid ] - YouTube https://www.youtube.com/watch?v=mx_AvrZK3TA
* Unreal Cel Shading - Distance Shadows - UE4 Tutorials #287 - YouTube https://www.youtube.com/watch?v=61SSIH3sDXo
* Shadow Shader - Community / Community & Industry Discussion - Epic Developer Community Forums https://forums.unrealengine.com/t/shadow-shader/4803
* Create a normal map inside Unreal 4? - Development / Rendering - Epic Developer Community Forums https://forums.unrealengine.com/t/create-a-normal-map-inside-unreal-4/10181
* Post Process Cartoon Outlines - Shader Graph Basics - Episode 57 - YouTube https://www.youtube.com/watch?v=Wpsqfpxb55Y

## Ink Shader Spec

Sept 23, 2023

The black and white treatment will include flat shading each material type and creating volume and shadow through the use of cross-hatching. An outline approach will also be used with the outlines having some wavering and thickness changes dependent on the type of edge it is representing. Below, find the detail for each shader type:

Cross-hatching:

An object-space shader (ie NOT screen space) that will vary on twos (every other frame) and feel hand-drawn, ideally with occasional breaks in the lines (this could be tricky). The cross-hatching will represent minimal form (possibly derived from shadows or normals, not clear yet). What this means is that the lines should reveal the shape of the underlying form, almost like contour lines. There will also be a cast shadow component to the cross-hatching. There should be 3-4 levels of cross-hatching:

* Level 1 - one set of narrowly spaced lines following the form of the object. 
* Level 2 - a second set of lines at 45º to the Level 1 lines
* Level 3 - a third set of lines at -45º to the Level 1 lines
* Level 4 - a fourth set of lines at 90º to the Level 1 lines

Shadows alone may not be enough information to generate these four levels, that’s why a combination normal and shadow shader might be required. 

Lines should have some thickness variation, but quite subtle. Ends of lines ideally can feather off, but not required.

Outlines:

Illustration style treats outlines differently depending on the context. Stylistically, there are six (maybe five) different treatments (mostly line thickness) that distinguish the outline style:

* 1 - Gross outline - the main outline that separates an object from its environment
* 2 - Overlap outline - where one object is on top of another object
* 3 - Material outline - where one material meets another material
* 4 - Normal outline - where an edge has normal variation over a certain threshold (like 30º, say)
* 5 - Overlap, same object - where one object overlaps itself (eg: fingers)
* 6 - Crease outline - (may be the same as the “Normal Outline”) where a surface has a crease (eye, smile, finger bend etc)

In the final frames, merge all of these based on various characteristics of each line type. Some may end with a dot. Some may feel more mechanical. Some may have more variation. Some may start/end with feathering.

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Roadmap

Expected to be completed November 17th, 2023.

## Author

Robin Rowe <robin.rowe@cinepaint.org>
323-535-0952 Los Angeles

## License

MIT open source.
