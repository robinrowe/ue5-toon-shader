# Shader Tutorial
Robin.Rowe@CinePaint.org

## Set-up

* Unreal Engine uses Unreal Shader (.usf) files
* Global shaders created in the Engine/Shaders folder
* Plugin shaders created in the Plugin/Shaders folder instead
* Set r.ShaderDevelopmentMode=1 in ConsoleVariables.ini file for detailed logs on shader compiles

## Create Global Shader

* Create MyShader.usf text file in Engine/Shaders folder under C:\Program Files (x86)\Epic Games\Launcher\Engine\Shaders
* Create MyShader.h
* Creade MyShaderPixelShader.h
* Create MyShader.cpp

For information on debugging the compilation of your .usf file and/or want to see the processed file, see Debugging the Shader Compiling Process. https://docs.unrealengine.com/4.27/en-US/ProgrammingAndScripting/Rendering/ShaderDevelopment/ShaderCompileProcess/

You can modify .usf files while an uncooked game or editor is running for quick iteration. Use the keyboard shortcut Ctrl + Shift + . (period), or open the console window and enter the command recompileshaders changed, to pick up and rebuild your shaders. 
