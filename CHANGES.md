UE5 Toon Shader CHANGES.md
Robin.Rowe@CinePaint.org

Weekly stand-ups on Tuesdays at 10am. Deadline to demo of Feb 9th, 2024. 

2024/1/30:

* Updated docs https://gitlab.com/robinrowe/ue5-toon-shader/-/blob/main/README.md
* Updated NVIDIA Quadro driver v550 https://www.nvidia.com/en-us/geforce/drivers/

2024/1/23:
2024/1/16:

* Updated UE5 Install v5.3.2

2023/9/30:

* Gitlab repo created https://gitlab.com/robinrowe/ue5-toon-shader/

2023/9/23:

* Project start
